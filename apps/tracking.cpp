#include "tracking.hpp"

void tracking(std::string filename) {
    
    //setup ttree readout
    std::vector<unsigned int>   *SuperID = 0;
	std::vector<unsigned short> *Bx      = 0;
	std::vector<unsigned short> *Link    = 0;
	std::vector<float> *Bend   = 0;
	std::vector<float> *LocalX = 0;
	std::vector<float> *LocalY = 0;
    
    TFile * f = new TFile(filename.c_str(), "READ");
    TTree* eventTree = (TTree*) f->Get("cbmsim");
    eventTree->SetBranchAddress("SuperID", &SuperID);
	eventTree->SetBranchAddress("Bx",      &Bx);
	eventTree->SetBranchAddress("Link",    &Link);
	eventTree->SetBranchAddress("Bend",    &Bend);
	eventTree->SetBranchAddress("LocalX",  &LocalX);
	eventTree->SetBranchAddress("LocalY",  &LocalY);

    Module modules[NMODULES]; //declaration of module

    //std::vector<Point> track;  
    
    for (int entry = 0; entry < eventTree->GetEntries(); ++entry) {
        if(entry==8) break;
        std::cout << "event #" << entry << std::endl;
        eventTree->GetEntry(entry);
        
        //filling the modules
        
        for (unsigned int j = 0; j < SuperID->size(); ++j) {
            Stub tmpStub;
            tmpStub.superID = SuperID->at(j);
            tmpStub.bend = Bend->at(j);
            tmpStub.bx = Bx->at(j);
            tmpStub.link = Link->at(j);
            tmpStub.localX = LocalX->at(j);
            tmpStub.localY = LocalY->at(j);
            modules[Link->at(j)].stubs.push_back(tmpStub);
        }

        bool GotX = false , GotY = false;
        
        std::vector<std::vector<Point>> points;  //free memory correct???
        std::vector<double> tmpStripX;
        std::vector<double> tmpStripY;
        
        const int NUM_BLOCKS = 3;
        points.resize(NUM_BLOCKS);
        
        Point tmpPoint;
        for (int link = 0; link < NMODULES; ++link) { // loop over all the modules        
            //if(modules[link].stubs.size() != 1) continue; 
             
            for(auto& stub: modules[link].stubs) {
                //std::cout << "inside stub loop" << std::endl;
                if(link==0 || link==2 || link==4){
                    tmpPoint.GetPhysicalX(stub, rot, GotX);
                    //std::cout << "got x: " << tmpPoint.x << std::endl;
                    tmpStripX.push_back(tmpPoint.x);
                }
                else if(link==1 || link==3 || link==5) { 
                    tmpPoint.GetPhysicalY(stub, rot, GotY);
                    //std::cout << "got y: " << tmpPoint.y << std::endl;
                    tmpStripY.push_back(tmpPoint.y);
                }
            }

            if(GotX==true && GotY==true){
                //std::cout << "gotX & gotY" << std::endl;
                double z_couple = (posZ[link - 1] + posZ[link])/2. ;
                //std::cout << "got x & y, size " << tmpStripX.size() << ", " << tmpStripY.size() << std::endl;
                for(unsigned int i = 0; i < tmpStripX.size(); i++){
                    for(unsigned int j = 0; j < tmpStripY.size(); j++){
                        //std::cout << "generating points" << std::endl;
                        Point p(tmpStripX[i], tmpStripY[j], z_couple);
                        points[(link-1)/2].push_back(p);
                        //p.print();
                    }
                }
                tmpStripX.clear(); 
                tmpStripY.clear(); 
                GotX = false;
                GotY = false;
            }   
        }
        std::cout << "END MODULES LOOP" << std::endl;

        int MissingPosition = -1;
        int NO_pts = 0;
        int TOT_pts = 0;
        for(int i = 0; i < NUM_BLOCKS; i++) {
            TOT_pts += points[i].size();
            if(points[i].size() == 0){
                NO_pts++;
                MissingPosition = i;
            }
            for(int j = 0; j < points[i].size(); j++) {
                points[i][j].print();
                std::cout << " ";
            }
        }
        std::cout << "Missing " << MissingPt << std::endl;
        if(NO_pts >= 2) {
            std::cout << "less than 2 points: skipping event\n";
            continue;
        }

        // prova con while i!=MissingPosition
        
        double min_dist = 999.;
        std::vector<double> distances;
        //std::vector<Line> track;
        Point trkpt_xy1;
        Point trkpt_xy2;
        Point trkpt_uv;

        //PROBLEM: ONLY GET ONE TRACK OUT OF THIS!!!
        for(unsigned int i = 0; i < NptsA; i++){
            for(unsigned int j = 0; j < NptsC; j++){
                line[i] = points[0]
                for(unsigned int k = 0; k < points[1].size(); k++){
                    
                    double dist = distancePointToLine(points[1][k], points[0][i], points[2][j]);
                    if(dist < min_dist) {
                        min_dist = dist;
                        trkpt_xy1 = points[0][i];
                        trkpt_uv  = points[1][k];
                        trkpt_xy2 = points[2][j];
                    }
                }
            }
        }
        
        // std::vector<Point> trk = {trkpt_xy1, trkpt_uv, trkpt_xy2};

        // std::string plane1 = "XZ";
        // double mXZ = 0;
        // double qXZ = 0;
        // double sigma_mXZ = 0;
        // double sigma_qXZ = 0;
        // LinearFit(trk, plane1, mXZ, qXZ, sigma_mXZ, sigma_qXZ);  //projection on XZ plane and linear fit
        
        // std::string plane2 = "YZ";
        // double mYZ = 0;
        // double qYZ = 0;
        // double sigma_mYZ = 0;
        // double sigma_qYZ = 0;
        // LinearFit(trk, plane2, mYZ, qYZ, sigma_mYZ, sigma_qYZ);  //projection on YZ plane and linear fit
        
        // // double FitPar[4] = {interceptXZ, slopeXZ, interceptYZ, slopeYZ};

        // std::cout << "slopeXZ: " << mXZ << " +/- " << sigma_mXZ << "; slopeYZ " << mYZ << " +/- " << sigma_mYZ << std::endl;
    
        // std::cout << "Writing in a file\n";
        // TFile* newFile = new TFile("track.root", "recreate");     
        // l->Write();
        // newFile->Close();
        
        
        for (int j = 0; j < NMODULES; ++j) {
            modules[j].stubs.clear();
        }
        for (std::vector<Point>& row : points) {
            row.clear();
        }
        std::cout << "END EVENT" << std::endl;
        std::cout << "*******************" << std::endl;
    }
}

int main(int argc, char *argv[]) {
    std::string filename = argv[1];
    tracking(filename);
    return 0;
}


// errore sui parametri di fit??????
// se ho un modulo sena punti come faccio a fare la retta con gli altri due????







        /*
        Point tmpPoint; 
        Stub mockStub;
        mockStub.localX = 0;
        mockStub.localY = 0.25;
        mockStub.link = 0;

        tmpPoint.GetPhysicalX(mockStub, rot, GotX);
        tmpPoint.GetPhysicalZ(mockStub, posZ);

        mockStub.link = 1;
        tmpPoint.GetPhysicalY(mockStub, rot, GotY);

        track.push_back(tmpPoint);

        Point tmpPoint2;
        mockStub.localX = 1016;
        mockStub.localY = 0.75;
        mockStub.link = 4;

        tmpPoint2.GetPhysicalX(mockStub, rot, GotX);
        tmpPoint2.GetPhysicalZ(mockStub, posZ);

        mockStub.link = 5;
        tmpPoint2.GetPhysicalY(mockStub, rot, GotY);
        
        track.push_back(tmpPoint2);

        std::cout << "lista punti: " << std::endl;
        track[0].print();
        track[1].print();

        *****************************

        Point tmpPoint1(0,0,0);
        Point tmpPoint2(0,0,2);
        Point tmpPoint(0,1,0);
        double dist = distancePointToLine(tmpPoint, tmpPoint1, tmpPoint2);
        std::cout << "distance = " << dist << std::endl;
        */