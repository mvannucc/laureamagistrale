#include <vector>
#include <cmath>
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <string>
#include <iostream>
#include <Eigen/Dense>

const int NMODULES = 6;
const int NSTRIPS = 1016;
const double STRIP_PITCH = 0.009;  // cm
const double FULL_DIMENSION = 10.;  //cm

double posZ [NMODULES] = {18.0218, 21.8693, 55.3635, 56.6205, 89.9218, 93.7693};
double rot [NMODULES][3] = {
    {3.1415, 0.233, 1.5708}, 
    {-0.233, 0, 0},
    {0, 0, 0.7854},
    {0, 3.1415, 0.7854},
    {3.1415, 0.233, 1.5708},
    {-0.233, 0, 0}
};

struct Stub {
    unsigned int superID;
    unsigned short bx;
    unsigned short link;
    float bend;
    float localX;
    float localY;
};

class Point{
public:
    double x, y, z;
    Point() : x(0), y(0), z(0) {};
    Point(double x, double y, double z) : x(x), y(y), z(z) {}
    
    void print() const {
        std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
    }

    void GetPhysicalX(Stub s, double rot[][3], bool &GotX){
        double posX = -999;
        if(s.link == 0 || s.link == 4){
            posX = (s.localX - 508) * STRIP_PITCH;
            posX = posX * cos(rot[s.link][1]); // rotation about Y axis
        }
        else if(s.link == 2){
            posX = (s.localX - 508) * STRIP_PITCH;
            posX = posX * cos(rot[s.link][2]); // rotation about Z axis
        }
        GotX = true;

        x = posX;       
    }

    void GetPhysicalY(Stub s, double rot[][3], bool &GotY){
        double posY = -999;
        
        if(s.link == 1 || s.link == 5){
            posY = (s.localX - 508) * STRIP_PITCH;
            posY = posY * cos(rot[s.link][0]); // rotation about X axis
        }
        else if(s.link == 3){
            posY = (s.localX - 508) * STRIP_PITCH;
            posY = posY * cos(rot[s.link][2]); // rotation about Z axis
        }
        GotY = true;

        y = posY;
    }

    // void GetPhysicalZ(Stub s, double posZ[]){
    //     z = (posZ[s.link -1] + posZ[s.link])/2.;
    // }
};


struct Module {
    std::vector<Stub> stubs;
};

// define the parametric line equation
// a parametric line is define from 6 parameters but 4 are independent
// x0,y0,z0,z1,y1,z1 which are the coordinates of two points on the line
// can choose z0 = 0 if line not parallel to x-y plane and z1 = 1;
class Line{
public:
    double x,y,z;
    Line() : x(0), y(0), z(0) {}; 
    Line(double t, const double *p){
        x = p[0] + p[1]*t;
        y = p[2] + p[3]*t;
        z = t;
    }
};

double distancePointToLine(Point point, Point linePoint1, Point linePoint2) {
    // Calculate the vector between the two line points
    Point lineVector = {linePoint2.x - linePoint1.x, linePoint2.y - linePoint1.y, linePoint2.z - linePoint1.z};

    // Calculate the vector from linePoint1 to the point
    Point pointToLinePoint1 = {point.x - linePoint1.x, point.y - linePoint1.y, point.z - linePoint1.z};

    // Calculate the scalar projection of pointToLinePoint1 onto lineVector
    double scalarProjection = (pointToLinePoint1.x * lineVector.x + pointToLinePoint1.y * lineVector.y + pointToLinePoint1.z * lineVector.z) /
                              (lineVector.x * lineVector.x + lineVector.y * lineVector.y + lineVector.z * lineVector.z);

    // Calculate the vector projection
    Point projection = {linePoint1.x + scalarProjection * lineVector.x,
                          linePoint1.y + scalarProjection * lineVector.y,
                          linePoint1.z + scalarProjection * lineVector.z};

    // Calculate the distance between the point and the projection
    double distance = sqrt((point.x - projection.x) * (point.x - projection.x) +
                           (point.y - projection.y) * (point.y - projection.y) +
                           (point.z - projection.z) * (point.z - projection.z));

    return distance;
}

void LinearFit(std::vector<Point> &pts, std::string plane, double &m, double &q, double &sigma_m, double &sigma_q){
    if(plane == "XZ"){
        double sumX = 0.0, sumZ = 0.0, sumZ2 = 0.0, sumXZ = 0.0;

        for(const Point& p : pts) {
            sumX += p.x; 
            sumZ += p.z; 
            sumZ2 += p.z * p.z; 
            sumXZ += p.x * p.z; 
        }

        int NPOINTS = pts.size();
        double delta = NPOINTS * sumZ2 - sumZ * sumZ;
        m = (NPOINTS * sumXZ - sumX * sumZ) / delta;
        q = (sumZ2 * sumX - sumZ * sumXZ) / delta;

        std::vector<double> residuals;
        for (const Point& p : pts) {
            double predictedX = m * p.z + q;
            double residual = p.x - predictedX;
            residuals.push_back(residual);
        }

        double sumSquaredResiduals = 0.0;
        for (double residual : residuals) {
            sumSquaredResiduals += residual * residual;
        }

        double sigmaX = std::sqrt(sumSquaredResiduals / (NPOINTS - 2)); 
        //double sigmaX = std::sqrt(varianceX + m * m * sigmaZ * sigmaZ); // uncomment if known z uncertainty
        sigma_m = sigmaX * std::sqrt(NPOINTS / delta);
        sigma_q = sigmaX * std::sqrt(sumZ2 / delta);
    }

    else if (plane == "YZ"){
        double sumY = 0.0, sumZ = 0.0, sumZ2 = 0.0, sumYZ = 0.0;

        for(const Point& p : pts) {
            sumY += p.y; 
            sumZ += p.z; 
            sumZ2 += p.z * p.z; 
            sumYZ += p.y * p.z; 
        }

        int NPOINTS = pts.size();
        double delta = NPOINTS * sumZ2 - sumZ * sumZ;
        m = (NPOINTS * sumYZ - sumY * sumZ) / delta;
        q = (sumZ2 * sumY - sumZ * sumYZ) / delta;

        std::vector<double> residuals;
        for (const Point& p : pts) {
            double predictedY = m * p.z + q;
            double residual = p.y - predictedY;
            residuals.push_back(residual);
        }

        double sumSquaredResiduals = 0.0;
        for (double residual : residuals) {
            sumSquaredResiduals += residual * residual;
        }

        double sigmaY = std::sqrt(sumSquaredResiduals / (NPOINTS - 2)); 
        //double sigmaY = std::sqrt(varianceY + m * m * sigmaZ * sigmaZ); // uncomment if known z uncertainty
        sigma_m = sigmaY * std::sqrt(NPOINTS / delta);
        sigma_q = sigmaY * std::sqrt(sumZ2 / delta);
    }
}