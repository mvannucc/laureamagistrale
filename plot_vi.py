from pymongo import MongoClient

client = MongoClient(f"mongodb://root:example@muedaq-supervisor.cern.ch")
db = client['module_tests']
collection = db["iv_curves"]
cursor = collection.find({"location": "B186"})
curves = [i for i in cursor]


