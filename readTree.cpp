#include <vector>

struct Stub {
    unsigned int superID;
    unsigned short bx;
    unsigned short link;
    float bend;
    float localX;
    float localY;
};

struct Module {
    std::vector<Stub> stubs;
};

const int NMODULES = 6;

void readTree(std::string filename) {
    
    //setup ttree readout
    std::vector<unsigned int>   *SuperID = 0;
	std::vector<unsigned short> *Bx      = 0;
	std::vector<unsigned short> *Link    = 0;
	std::vector<float> *Bend   = 0;
	std::vector<float> *LocalX = 0;
	std::vector<float> *LocalY = 0;
    
    TFile * f = new TFile(filename.c_str(), "READ");
    TTree* eventTree = (TTree*) f->Get("cbmsim");
    eventTree->SetBranchAddress("SuperID", &SuperID);
	eventTree->SetBranchAddress("Bx",      &Bx);
	eventTree->SetBranchAddress("Link",    &Link);
	eventTree->SetBranchAddress("Bend",    &Bend);
	eventTree->SetBranchAddress("LocalX",  &LocalX);
	eventTree->SetBranchAddress("LocalY",  &LocalY);

    Module modules[NMODULES]; //declaration of module

    const int nbins = 200;
    //local x histograms
    std::vector<TH1F*> hmod;
    for(int i=0; i<NMODULES; i++) {
       hmod.push_back(new TH1F(("m" + std::to_string(i)).c_str(), ("Modulo " + std::to_string(i) + ": Local X").c_str(), nbins, 0, 1024));
    }

    const int nbins_b = 40;
    //bend histograms
    std::vector<TH1F*> hbend;
    for(int i=0; i<NMODULES; i++) {
       hbend.push_back(new TH1F(("b" + std::to_string(i)).c_str(), ("Modulo " + std::to_string(i) + ": bend").c_str(), nbins_b, -10, 10));
    } 

    //correlation LocalX-bend histograms
    std::vector<TH2F*> hcorr_x_bend;
    for(int i=0; i<NMODULES; i++) {
       hcorr_x_bend.push_back(new TH2F(("x_b" + std::to_string(i)).c_str(), ("Modulo " + std::to_string(i) + ": x_b").c_str(), nbins, 0, 1024, nbins_b, -10, 10));
    }

    //beam profile histograms
    std::vector<std::vector<TH2F*>> hprof;
    for(int i=0; i<NMODULES; i++) {
        std::vector<TH2F*> tmpVec;
        for(int j=0; j<NMODULES; j++) {
            tmpVec.push_back(new TH2F(("m" + std::to_string(i) + std::to_string(j)).c_str(), ("m" + std::to_string(i) + std::to_string(j)).c_str() , 200, 0, 1024, 200, 0, 1024));
        }
        hprof.push_back(tmpVec);
    }

    // stubs number vs time graphs
    // std::vector<TGraph*> TimeGraph {
    //     new TGraph(),
    //     new TGraph(),
    //     new TGraph(),
    //     new TGraph(),
    //     new TGraph(),
    //     new TGraph()
    // };

    int nStub[NMODULES] = {0};
    
    for (int entry = 0; entry < eventTree->GetEntries(); ++entry) {
        if(entry==1e4) break;
        eventTree->GetEntry(entry);
        
        //filling the modules
        
        for (int j = 0; j < SuperID->size(); ++j) {
            Stub tmpStub;
            tmpStub.superID = SuperID->at(j);
            tmpStub.bend = Bend->at(j);
            tmpStub.bx = Bx->at(j);
            tmpStub.link = Link->at(j);
            tmpStub.localX = LocalX->at(j);
            tmpStub.localY = LocalY->at(j);
            modules[Link->at(j)].stubs.push_back(tmpStub);
        }

        //Here do your computation
        //Example of what you can do with the modules
        for (int j = 0; j < NMODULES; ++j) { // loop over all the modules
            //std::cout << "Module with link " << j << " has " << modules[j].stubs.size() << " stubs" << std::endl;

            //to access the ith stub in the module - this can cause errors if you try to access a stub that doesn't exist
            if(modules[j].stubs.size() == 0) continue;
            //std::cout << "Stub 0 in module " << j << " has localX: " <<  modules[j].stubs.at(0).localX << std::endl; 
            
            nStub[j] = nStub[j] + modules[j].stubs.size();

            for(auto& stub1: modules[j].stubs) {
                hmod[j]->Fill(stub1.localX);
                hbend[j]->Fill(stub1.bend);
                hcorr_x_bend[j]->Fill(stub1.localX, stub1.bend);

                // TimeGraph[j]->AddPoint((stub1.superID * 3564 + stub1.bx) * 25, modules[j].stubs.size());
                // TimeGraph[j]->SetName(("st" + std::to_string(j)).c_str());
                // TimeGraph[j]->GetXaxis()->SetTitle("Time [ns]");
                // TimeGraph[j]->GetYaxis()->SetTitle("Stubs");
                // TimeGraph[j]->SetTitle(("Module " + std::to_string(j)).c_str());

                //prendiamo il secondo modulo
                for(int k = 0; k < NMODULES; ++k) {
                    for(auto& stub2: modules[k].stubs){
                        hprof[j][k]->Fill(stub1.localX, stub2.localX, 1./modules[j].stubs.size());  // scelta peso?
                    }
                }
            }
            
            //you can also copy a stub with
            //Stub tmpStub = modules[j].stubs.at(0);
            //now tmpStub is a copy of the first stub in the module 
        }

        for (int j = 0; j < NMODULES; ++j) {
            modules[j].stubs.clear();
        }  
    }

    std::cout << "Writing in a file\n";
    TFile* newFile = new TFile("newfile.root", "recreate");
    for(auto& hm: hmod){
        hm->Write();
    }
    for(auto& hb: hbend){
        hb->Write();
    }
    for(auto& hxb: hcorr_x_bend){
        hxb->Write();
    }
    for(auto& vh: hprof){
        for(auto& h: vh){
            h->Write();
        }
    }
    // for(auto& tg: TimeGraph){
    //     tg->Write();
    // }
    newFile->Close();
}